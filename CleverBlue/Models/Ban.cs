﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleverBlue.Models
{
    public class Ban
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string Reason { get; set; }

    }
}
