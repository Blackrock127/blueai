﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleverBlue.Models
{
    public class Modules
    {
        //IDENTIFIER

        public Guid Id { get; set; }
        public Guid ApiKey { get; set; }

        //SETTINGS

        public bool JurassicAPI { get; set; }
        public bool PrehistoricSync { get; set; }
        public bool BlueAI { get; set; }
        public bool AutoBan { get; set; }
        public bool Welcome { get; set; }
        public bool Leave { get; set; }
        public bool BackOnlineWelcome { get; set; }
        public bool GoogleSearch { get; set; }
        public bool Youtube { get; set; }
        public bool ModerationCmds { get; set; }
        public bool FunCmds { get; set; }
        public bool CustomModules { get; set; }
    }
}
