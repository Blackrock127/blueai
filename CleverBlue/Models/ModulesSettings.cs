﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleverBlue.Models
{
    public class ModulesSettings
    {
        //IDENTIFIER

        public Guid Id { get; set; }
        public Guid ApiKey { get; set; }

        //SETTINGS
        public string ModuleName { get; set; }
        public string ModuleOptions { get; set; }
    }
}
