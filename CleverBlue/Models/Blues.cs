﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleverBlue.Models
{
    public class Blues
    {
        public Guid Id { get; set; }

        public BlueUser Owner { get; set; }
        
        public string OwnerId { get; set; } // look

        public string Guild { get; set; }

        public string GuildName { get; set; }

        public Guid ModulesId { get; set; }

        public string ApiKey { get; set; }

        public string BlueVersion { get; set; }

        public bool IsDisabled { get; set; }

        public int InstallationStep { get; set; }
    }
}
