﻿using System;
using System.Collections.Generic;
using System.Text;
using CleverBlue.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CleverBlue.Data
{
    public class ApplicationDbContext : IdentityDbContext<BlueUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
