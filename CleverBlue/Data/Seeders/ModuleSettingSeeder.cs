﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CleverBlue.Models;
using CleverBlue.Data;
namespace CleverBlue.Data.Seeders
{
    public class ModuleSettingSeeder
    {
        private CleverBlueContext _context;
        public ModuleSettingSeeder(CleverBlueContext context) // ctor
        {
            _context = context;
        }
        public async Task AddModuleSettings(Guid apiKey)
        {
            var modulesSettings = new[]
            {
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "AutoBan",
                    ModuleOptions = "",
                },
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "BackOnlineWelcome",
                    ModuleOptions = "",
                },
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "BlueAI",
                    ModuleOptions = "",
                },
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "CustomModules",
                    ModuleOptions = "",
                },
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "FunCmds",
                    ModuleOptions = "",
                },
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "GoogleSearch",
                    ModuleOptions = "",
                },
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "PrehistoricSync",
                    ModuleOptions = "",
                },
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "Welcome",
                    ModuleOptions = "",
                },
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "Leave",
                    ModuleOptions = "",
                },
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "Youtube",
                    ModuleOptions = "",
                },
                new ModulesSettings()
                {
                    ApiKey = apiKey,
                    ModuleName = "ModerationCmds",
                    ModuleOptions = "",
                }

            };
            _context.ModulesSettings.AddRange(modulesSettings);
            await _context.SaveChangesAsync();
        }
    }
}
