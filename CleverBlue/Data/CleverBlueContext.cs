﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CleverBlue.Models
{
    public class CleverBlueContext : DbContext
    {
        public CleverBlueContext (DbContextOptions<CleverBlueContext> options) : base(options)
        {

        }

        public DbSet<CleverBlue.Models.Ban> Bans { get; set; }
        public DbSet<CleverBlue.Models.Blues> Blues { get; set; }

        //MODULES SYSTEM

        public DbSet<CleverBlue.Models.Modules> Modules { get; set; }
        public DbSet<CleverBlue.Models.ModulesSettings> ModulesSettings { get; set; }

    }
}
