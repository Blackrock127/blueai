﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CleverBlue.Models;

namespace CleverBlue.Views.DB.DBlues
{
    public class BluesController : Controller
    {
        private readonly CleverBlueContext _context;

        public BluesController(CleverBlueContext context)
        {
            _context = context;
        }

        // GET: Blues
        public async Task<IActionResult> Index()
        {
            return View(await _context.Blues.ToListAsync());
        }

        // GET: Blues/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blues = await _context.Blues
                .FirstOrDefaultAsync(m => m.Id == id);
            if (blues == null)
            {
                return NotFound();
            }

            return View(blues);
        }

        // GET: Blues/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Blues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,OwnerId,Guild,GuildName,ModulesId,ApiKey,BlueVersion,IsDisabled")] Blues blues)
        {
            if (ModelState.IsValid)
            {
                blues.Id = Guid.NewGuid();
                _context.Add(blues);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(blues);
        }

        // GET: Blues/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blues = await _context.Blues.FindAsync(id);
            if (blues == null)
            {
                return NotFound();
            }
            return View(blues);
        }

        // POST: Blues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,OwnerId,Guild,GuildName,ModulesId,ApiKey,BlueVersion,IsDisabled")] Blues blues)
        {
            if (id != blues.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(blues);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BluesExists(blues.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(blues);
        }

        // GET: Blues/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blues = await _context.Blues
                .FirstOrDefaultAsync(m => m.Id == id);
            if (blues == null)
            {
                return NotFound();
            }

            return View(blues);
        }

        // POST: Blues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var blues = await _context.Blues.FindAsync(id);
            _context.Blues.Remove(blues);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BluesExists(Guid id)
        {
            return _context.Blues.Any(e => e.Id == id);
        }
    }
}
