﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using CleverBlue.Models;
using Microsoft.AspNetCore.Mvc;
using CleverBlue.Data;
using CleverBlue.Data.Seeders;
using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace CleverBlue.Controllers
{

    public class DashboardController : Controller
    {
        private readonly UserManager<BlueUser> _userManager;
        private readonly CleverBlueContext _bluesManager;
        private readonly ModuleSettingSeeder _seeder;
        private readonly Guid _apikey;

        private readonly string[] _know = {
                "Velociraptors hunted in a group?", "Blue is a velociraptor?",
                "Velociraptors arent in the jurassic period!", "BlueAI was called blue due do cleverness."/*, "BlueAI is dumb af"*/
            }; // there


        // public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "API")]
            public string Api { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Guild ID")]
            public string GuildId { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Server Owner ID")]
            public bool ServerOwnerId { get; set; }
        }

        public DashboardController(UserManager<BlueUser> userManager, CleverBlueContext context,
            ModuleSettingSeeder seeder)
        {
            _userManager = userManager;
            _bluesManager = context;
            // and then set it
            _seeder = seeder;
            _apikey = Guid.NewGuid();
            

        }

        public async Task<IActionResult> Index()
        {
            

            var user = await _userManager.GetUserAsync(HttpContext.User);
            var blue = _bluesManager.Blues.FirstOrDefault(x => x.Owner == user);

            //NEW


            if (blue != null && !blue.IsDisabled)
            {
                return RedirectToAction("DashboardMain");
            }
            else
            {
                //INSTALLATION
                return RedirectToAction("BlueSetup");
            }
        }

        public async Task<IActionResult> DashboardMain()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var blue = _bluesManager.Blues.FirstOrDefault(x => x.Owner == user);
            return View("Index");
            
        }

        public async Task<IActionResult> BlueSetup()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var blue = _bluesManager.Blues.FirstOrDefault(x => x.Owner == user);

            if (blue == null)
            {
                //DID YOU KNOW SECTION
                Random rand = new Random();
                int index = rand.Next(_know.Length);
                ViewData["Know"] = _know[index];
                ViewData["api"] = _apikey;

                return View("Setup");
            }
            else
            {
                return RedirectToAction("CreateBlueNf");
            }
        }


        //NO FORM
        public async Task<IActionResult> CreateBlueNf()
        {

            var user = await _userManager.GetUserAsync(HttpContext.User);
            var blue = _bluesManager.Blues.FirstOrDefault(x => x.Owner == user);


            if (blue != null && blue.IsDisabled)
            {
                //DID YOU KNOW SECTION
                /*Random rand = new Random();
                int index = rand.Next(_know.Length);
                ViewData["Know"] = _know[index];*/

                ViewData["keyApi"] = blue.ApiKey;

                //VIEW
                if (blue.InstallationStep == 1)
                {
                    // we can just use our blue
                    blue.InstallationStep = 2;
                    _bluesManager.SaveChanges();

                    ViewData["PV"] = "8";
                    ViewData["PS"] = "Preparing 𝓑𝓵𝓾𝓮...";
                    ViewData["PD"] = "success";
                }
                else if (blue.InstallationStep == 2)
                {
                    blue.InstallationStep = 3;
                    _bluesManager.SaveChanges();

                    Modules m1 = new Modules()
                    {
                        ApiKey = Guid.Parse(blue.ApiKey),
                        PrehistoricSync = true,
                        Welcome = true,
                        Leave = true,
                        ModerationCmds = true,
                        FunCmds = false,
                        Youtube = false,
                        BlueAI = false,
                        GoogleSearch = false,
                        CustomModules = false,
                        BackOnlineWelcome = false,
                        AutoBan = false,
                        JurassicAPI = true
                    };


                    _bluesManager.Modules.Add(m1);

                    await _bluesManager.SaveChangesAsync();

                    ViewData["PV"] = "58";
                    ViewData["PS"] = "Installing Modules...";
                    ViewData["PD"] = "success";
                }
                else if (blue.InstallationStep == 3)
                {
                    //ModulesSettings Array 
                    blue.InstallationStep = 4;
                    await _seeder.AddModuleSettings(Guid.Parse(blue.ApiKey));

                    await _bluesManager.SaveChangesAsync();

                    ViewData["PV"] = "87";
                    ViewData["PS"] = "Preparing Settings...";
                    ViewData["PD"] = "success";
                }
                else if (blue.InstallationStep == 4)
                {

                    ViewData["PV"] = "100";
                    ViewData["PS"] = "Getting Ready...";
                    ViewData["PD"] = "success";

                    blue.InstallationStep = 5;
                    blue.IsDisabled = false;
                    _bluesManager.SaveChanges();

                    return RedirectToAction("Index");

                }


                return View("Installation");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        //FORM
        [Route("Dashboard/CreateBlue/Send")]
        [HttpPost] // say it's post
        public async Task<IActionResult> CreateBlue(InputModel input)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var blue = _bluesManager.Blues.FirstOrDefault(x => x.Owner == user);

            //NEW


            if (blue != null && blue.IsDisabled == false)
            {
                return RedirectToAction("Index");
            }
            else
            {
                //DID YOU KNOW SECTION
                /*Random rand = new Random();
                int index = rand.Next(_know.Length);
                ViewData["Know"] = _know[index];*/

                //FORM SUBMISSION

                Blues newBlue = new Blues()
                {

                    ApiKey = _apikey.ToString(),
                    BlueVersion = "PRO",
                    GuildName = "Unknown",
                    Guild = input.GuildId, // errawr errawrr? rawr
                    InstallationStep = 1,
                    IsDisabled = true,
                    Owner = user
                };

                _bluesManager.Blues.Add(newBlue);
                await _bluesManager.SaveChangesAsync();

                return RedirectToAction("CreateBlueNf");
            }

        }

        //DASHBOARD PAGES

    }
}