﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CleverBlue.Migrations
{
    public partial class Blues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bans",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Reason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BlueUser",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    NormalizedUserName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    NormalizedEmail = table.Column<string>(nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlueUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Modules",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ApiKey = table.Column<Guid>(nullable: false),
                    JurassicAPI = table.Column<bool>(nullable: false),
                    PrehistoricSync = table.Column<bool>(nullable: false),
                    BlueAI = table.Column<bool>(nullable: false),
                    AutoBan = table.Column<bool>(nullable: false),
                    Welcome = table.Column<bool>(nullable: false),
                    Leave = table.Column<bool>(nullable: false),
                    BackOnlineWelcome = table.Column<bool>(nullable: false),
                    GoogleSearch = table.Column<bool>(nullable: false),
                    Youtube = table.Column<bool>(nullable: false),
                    ModerationCmds = table.Column<bool>(nullable: false),
                    FunCmds = table.Column<bool>(nullable: false),
                    CustomModules = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Modules", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ModulesSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ApiKey = table.Column<Guid>(nullable: false),
                    ModuleName = table.Column<string>(nullable: true),
                    ModuleOptions = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModulesSettings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Blues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    OwnerId = table.Column<string>(nullable: true),
                    Guild = table.Column<string>(nullable: true),
                    GuildName = table.Column<string>(nullable: true),
                    ModulesId = table.Column<Guid>(nullable: false),
                    ApiKey = table.Column<string>(nullable: true),
                    BlueVersion = table.Column<string>(nullable: true),
                    IsDisabled = table.Column<bool>(nullable: false),
                    InstallationStep = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Blues_BlueUser_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "BlueUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Blues_OwnerId",
                table: "Blues",
                column: "OwnerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bans");

            migrationBuilder.DropTable(
                name: "Blues");

            migrationBuilder.DropTable(
                name: "Modules");

            migrationBuilder.DropTable(
                name: "ModulesSettings");

            migrationBuilder.DropTable(
                name: "BlueUser");
        }
    }
}
