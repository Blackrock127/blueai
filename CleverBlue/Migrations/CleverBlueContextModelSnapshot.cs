﻿// <auto-generated />
using System;
using CleverBlue.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CleverBlue.Migrations
{
    [DbContext(typeof(CleverBlueContext))]
    partial class CleverBlueContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("CleverBlue.Models.Ban", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Reason");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.ToTable("Bans");
                });

            modelBuilder.Entity("CleverBlue.Models.BlueUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<string>("Email");

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail");

                    b.Property<string>("NormalizedUserName");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.ToTable("BlueUser");
                });

            modelBuilder.Entity("CleverBlue.Models.Blues", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApiKey");

                    b.Property<string>("BlueVersion");

                    b.Property<string>("Guild");

                    b.Property<string>("GuildName");

                    b.Property<int>("InstallationStep");

                    b.Property<bool>("IsDisabled");

                    b.Property<Guid>("ModulesId");

                    b.Property<string>("OwnerId");

                    b.HasKey("Id");

                    b.HasIndex("OwnerId");

                    b.ToTable("Blues");
                });

            modelBuilder.Entity("CleverBlue.Models.Modules", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ApiKey");

                    b.Property<bool>("AutoBan");

                    b.Property<bool>("BackOnlineWelcome");

                    b.Property<bool>("BlueAI");

                    b.Property<bool>("CustomModules");

                    b.Property<bool>("FunCmds");

                    b.Property<bool>("GoogleSearch");

                    b.Property<bool>("JurassicAPI");

                    b.Property<bool>("Leave");

                    b.Property<bool>("ModerationCmds");

                    b.Property<bool>("PrehistoricSync");

                    b.Property<bool>("Welcome");

                    b.Property<bool>("Youtube");

                    b.HasKey("Id");

                    b.ToTable("Modules");
                });

            modelBuilder.Entity("CleverBlue.Models.ModulesSettings", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("ApiKey");

                    b.Property<string>("ModuleName");

                    b.Property<string>("ModuleOptions");

                    b.HasKey("Id");

                    b.ToTable("ModulesSettings");
                });

            modelBuilder.Entity("CleverBlue.Models.Blues", b =>
                {
                    b.HasOne("CleverBlue.Models.BlueUser", "Owner")
                        .WithMany()
                        .HasForeignKey("OwnerId");
                });
#pragma warning restore 612, 618
        }
    }
}
