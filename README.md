![Scheme](http://prehistorictek.com/logo-ps.svg)  

[![Build](https://img.shields.io/badge/Build-Success-green.svg?style=flat-square)]()
[![Issues](https://img.shields.io/badge/Issues-38-c62828.svg?style=flat-square)]()
[![RexAPI](https://img.shields.io/badge/Xtreme-v1.2-2196f3.svg?style=flat-square)]()
[![Release Date](https://img.shields.io/badge/Release%20Date-July%202019-green.svg?style=flat-square)]()

## About Blue AI
----------

Rex Tracker is an ARK: Survival Evolved Toolkit as a Web Application! It Includes a Taming Calculator, Breeding Calculator and Dino Network.

## How to install
----------

> **Requirements :**

> - Dotnet Library 
> - PHP 7.0
> - Nginx
> - Postgres 
> - Xtreme 1.2
>

    git pull --all

##Configuration
----------
Theme Configuration :

    php artisan rex:theme "Default"

Database Configuration :

    php artisan migrate

  Admin Configuration **REQUIRED**
> **Note :**
> This configuration is required to get access to the admin dashboard went you install Rex  Tracker if you do not configure the Rex Tracker Admin, Rex Tracker wont be able to execute any commands.



    php artisan rex:create_admin "Admin Username" "Admin Password" "Admin Email"

##Rex API
----------
![Scheme]()

Rex API is a powerful API that can control the entire website using json a few commands are available too. Rex API is used for the Android and Windows Version of Rex Tracker.

Version of Rex API (Command) :

    php artisan rexapi --version

The Rex API is currently Uncompleted and very Unstable!

## How to push and pull
----------

Push:

    git add .
    git commit -m "Commit Message"
    git push origin master
    git push -f origin master **FORCE PUSH ONLY**

Pull:

    git pull --all


##Patch Notes
----------

**Patch Notes of Rex Tracker v4.17**

- Added New Admin Management System

- Added Element Calculator

- Added Forge Calculator

- Added Mortar Calculator

- Added Turret Refill Timers

- Added Custom Timers

- Added Dino Network

- Added New Security System

- Fixed RexAPI

- Added new dashboard style

- Added Logs

- Fixed Many Bugs

- Added Optimization

- Added new features in RexAPI

- Added new Delete Warning Dialog

- Added Private Server Option

- Added Skip Creating Tribe Option
